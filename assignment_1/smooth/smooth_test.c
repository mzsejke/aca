#include <stdio.h>

// #define N_SAMPLES	5
// #define N_COEFFS	3

// double	sample[N_SAMPLES] = {1, 2, 1, 2, 1};
// double	coeff[N_COEFFS]= {0.5, 1, 0.5};
// double	result[N_SAMPLES];


// #define N_SAMPLES	3
// #define N_COEFFS	3

// double	sample[N_SAMPLES] = {9, 8, 1};
// double	coeff[N_COEFFS]= {1, 1, 9};
// double	result[N_SAMPLES];



// #define N_SAMPLES	100
// #define N_COEFFS	3

// double	sample[N_SAMPLES] = {1, 6, 2, 8, 5, 1, 6, 2, 8, 5,
// 							 2, 6, 2, 8, 5, 1, 6, 2, 8, 5,
// 							 3, 6, 2, 8, 5, 1, 6, 2, 8, 5,
// 							 4, 6, 2, 8, 5, 1, 6, 2, 8, 5,
// 							 5, 6, 2, 8, 5, 1, 6, 2, 8, 5,
// 							 6, 6, 2, 8, 5, 1, 6, 2, 8, 5,
// 							 7, 6, 2, 8, 5, 1, 6, 2, 8, 5,
// 							 8, 6, 2, 8, 5, 1, 6, 2, 8, 5,
// 							 9, 6, 2, 8, 5, 1, 6, 2, 8, 5,
// 							 10, 6, 2, 8, 5, 1, 6, 2, 8, 5};
// double	coeff[N_COEFFS]= {0.2, 2, 1};
// double	result[N_SAMPLES];



// #define N_SAMPLES	8
// #define N_COEFFS	3

// double	sample[N_SAMPLES] = {1, 2, 1, 2, 1, 1, 2, 1};
// double	coeff[N_COEFFS]= {-2, -1, 0.5};
// double	result[N_SAMPLES];



// #define N_SAMPLES	25
// #define N_COEFFS	3

// double	sample[N_SAMPLES] = {-1, 2, 1, 2, 1,
// 							 1, -2, 1, 2, 1,
// 							 1, 2, -1, 2, 1,
// 							 1, 2, 1, -2, 1,
// 							 1, 2, 1, 2, -1};
// double	coeff[N_COEFFS]= {0.5, 1, 0.5};
// double	result[N_SAMPLES];



#define N_SAMPLES	17
#define N_COEFFS	3

double	sample[N_SAMPLES] = {9, 8, 7, 6, 5, 6, 7, 6, 5, 4, 3, 2, 3, 4, 5, 6, 7};
double	coeff[N_COEFFS]= {12, 20, 23};
double	result[N_SAMPLES];


void smooth(double sample[], double coeff[], double result[], int n)
{
	int i, j;
	double norm=0.0;

	for (i=0; i<N_COEFFS; i++)
		norm+= coeff[i]>0 ? coeff[i] : -coeff[i];

	for (i=0; i<n; i++){
		if (i==0 || i==n-1){
			result[i] = sample[i];
		}else{
			result[i]=0.0;
			for (j=0; j<N_COEFFS; j++)
				result[i] += sample[i-1+j]*coeff[j];
			result[i]/=norm;
		}
	}
}

int main(int argc, char *arvg[])
{
	int i;

	if (N_SAMPLES>=N_COEFFS)
		smooth(sample, coeff, result, N_SAMPLES);

	for (i=0; i<N_SAMPLES; i++)
		printf("%f\n", result[i]);
}
