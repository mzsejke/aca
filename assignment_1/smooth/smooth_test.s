            .data

# CR:         .word32 0x10000
# DR:         .word32 0x10008
N_COEFFS:   .word 3


coeff:      .double 0.5, 1.0, 0.5
N_SAMPLES:  .word 5
sample:     .double 1.0, 2.0, 1.0, 2.0, 1.0
result:     .space 5

# coeff:      .double 1, 1, 9
# N_SAMPLES:  .word 3
# sample:     .double 9, 8, 1
# result:     .space 3

# coeff:      .double 0.2, 2, 1
# N_SAMPLES:  .word 100
# sample:     .double 1, 6, 2, 8, 5, 1, 6, 2, 8, 5
# sample1:    .double 2, 6, 2, 8, 5, 1, 6, 2, 8, 5
# sample2:    .double 3, 6, 2, 8, 5, 1, 6, 2, 8, 5
# sample3:    .double 4, 6, 2, 8, 5, 1, 6, 2, 8, 5
# sample4:    .double 5, 6, 2, 8, 5, 1, 6, 2, 8, 5
# sample5:    .double 6, 6, 2, 8, 5, 1, 6, 2, 8, 5
# sample6:    .double 7, 6, 2, 8, 5, 1, 6, 2, 8, 5
# sample7:    .double 8, 6, 2, 8, 5, 1, 6, 2, 8, 5
# sample8:    .double 9, 6, 2, 8, 5, 1, 6, 2, 8, 5
# sample9:    .double 10, 6, 2, 8, 5, 1, 6, 2, 8, 5
# result:     .space 100

# coeff:      .double -2, -1, 0.5
# N_SAMPLES:  .word 8
# sample:     .double 1, 2, 1, 2, 1, 1, 2, 1
# result:     .space 8

# coeff:      .double 0.5, 1, 0.5
# N_SAMPLES:  .word 25
# sample:     .double -1, 2, 1, 2, 1, 1, -2, 1, 2, 1, 1, 2, -1, 2, 1
# sample1:    .double 1, 2, 1, -2, 1, 1, 2, 1, 2, -1
# result:     .space 25

# coeff:      .double 12, 20, 23
# N_SAMPLES:  .word 17
# sample:     .double 9, 8, 7, 6, 5, 6, 7, 6, 5, 4, 3, 2, 3, 4, 5, 6, 7
# result:     .space 17

            .text
start:
			; Check if NSAMPLES >= 3
			ld R1, N_SAMPLES(R0)  	 	# R1 --> Samples
			; Make F0 = 0
			mtc1  R0, F0
			slti R5, R1, 3				# R5 --> Temp LT
			daddui R2, R0, coeff
			bnez R5, exit		
			l.d F1, 0(R2)  				# F1 --> coeff0

			; Sum the absolute values
			l.d F2, 8(R2)     			# F2 --> coeff1
			c.le.d  F1, F0
			bc1f    sub_coef2
			l.d     F3, 16(R2)    	 	# F3 --> coeff2
			sub.d   F1, F0, F1
sub_coef2:  c.le.d  F2, F0
			bc1f    sub_coef3
			daddi   R3, R1, -2	    	# R3 --> Samples - 2 
			sub.d   F2, F0, F2
sub_coef3:  c.le.d  F3, F0
            bc1f    add_coef
			daddi   R4, R0, sample+8  	# R4 --> sample_add
			sub.d   F3, F0, F3
add_coef:   add.d   F4, F1, F2
			l.d		F1, 0(R2)  			# F1 --> coeff0
			daddi   R1, R1, -1			# R1 --> Samples - 1
			l.d 	F2, 8(R2)     		# F2 --> coeff1
			add.d   F4, F4, F3          # F4 --> norm

			l.d     F3, 16(R2)    	 	# F3 --> coeff2
			daddi   R2, R0, result+8  	# R2 --> result_add

result:		
			dsll R1, R1, 3          # R1 --> (samples - 1) * 8

			; First result is always same as sample
			l.d F5, sample(R0)		# F5 --> sample[0] 
			s.d F5, result(R0)

			daddi R1, R1, sample	# R1 --> sample_end

			andi R5, R3, 0x03       # R5 --> (Samples - 2) % 4
			dsll R3, R5, 3			# R3 --> mod * 8

			daddi R7, R0, 1
			daddi R8, R0, 2
			
			l.d F6, -8(R4)   		# F6 --> sample[i-1]
			l.d F8,  0(R4)			# F8 --> sample[i]

			beq R5, R7, smooth_one
			l.d F10, 8(R4)			# F10 --> sample[i+1]

			beq R5, R8, smooth_two
			l.d F13, 16(R4)   		# F13 --> sample[i+2]

			bnez R5, smooth_three
			l.d F18, 24(R4)   		# F18 --> sample[i+3]
			l.d F23, 32(R4)   		# F23 --> sample[i+4]

loop:		
			daddi R3, R0, 32  		# R3 --> 4

			mul.d F24, F13, F1      # F24 --> temp_result = sample[i+2] * coef0
			mul.d F25, F18, F2     	# F25 --> temp_mul1 = sample[i+3] * coef1
			mul.d F26, F23, F3      # F26 --> temp_mul2 = sample[i+4] * coef2

			l.d F10, 8(R4)			# F10 --> sample[i+1] 
			l.d F8,  0(R4)			# F8 --> sample[i]
			
			add.d F24, F24, F25    	# F24 --> temp_result += temp_mul1
			
			l.d F6, -8(R4)   		# F6 --> sample[i-1]
			
			add.d F24, F24, F26     # F24 --> temp_result += temp_mul2

smooth_three:

			mul.d F19, F10, F1      # F19 --> temp_result = sample[i+1] * coef0
			mul.d F20, F13, F2     	# F20 --> temp_mul1 = sample[i+2] * coef1
			mul.d F21, F18, F3      # F21 --> temp_mul2 = sample[i+3] * coef2
			
			div.d F27, F24, F4      # F27 --> result = temp_result / norm

			add.d F19, F19, F20    	# F19 --> temp_result += temp_mul1
			
			s.d F27, 24(R2)
			
			add.d F19, F19, F21     # F19 --> temp_result += temp_mul2
	

smooth_two:
			mul.d F14, F8, F1      	# F14 --> temp_result = sample[i] * coef0
			mul.d F15, F10, F2     	# F15 --> temp_mul1 = sample[i+1] * coef1
			mul.d F16, F13, F3      # F16 --> temp_mul2 = sample[i+2] * coef2
			
			div.d F22, F19, F4      # F22 --> result = temp_result / norm
		
			add.d F14, F14, F15    	# F14 --> temp_result += temp_mul1
		
			s.d F22, 16(R2)
		
			add.d F14, F14, F16     # F14 --> temp_result += temp_mul2

smooth_one:
			mul.d F7, F6, F1      	# F7 --> temp_result = sample[i-1] * coef0
			mul.d F9, F8, F2     	# F9 --> temp_mul1 = sample[i] * coef1
			mul.d F11, F10, F3      # F11 --> temp_mul2 = sample[i+1] * coef2

			div.d F17, F14, F4      # F17 --> result = temp_result / norm

			add.d F7, F7, F9      	# F7 --> temp_result += temp_mul1

			s.d F17, 8(R2)

			add.d F7, F7, F11      	# F7 --> temp_result += temp_mul2

			dadd R4, R4, R3  		# R4 --> sample_addr

			div.d F12, F7, F4      	# F12 --> result = temp_result / norm
			
			l.d F13, 16(R4)   		# F13 --> sample[i+2]
			l.d F18, 24(R4)   		# F18 --> sample[i+3]
			l.d F23, 32(R4)   		# F23 --> sample[i+4]
			s.d F12, 0(R2)
			
			bne    R4, R1, loop
			dadd R2, R2, R3  		# R2 --> result_addr


			l.d F5, 0(R4)
			s.d F5, 0(R2)

exit:		halt

# 			ld R4, N_SAMPLES(r0)   	# R1 --> Samples
# 			dsll R4, R4, 3
# 			daddi R4, R4, result
# 			daddi R1, R0, result 
# printloop:
# 	        beq R1, R4, exit
# 	        j printdouble
# 			nop

# printdouble:
# 	        l.d f0, 0(r1)
# 	        lwu r11,CR(r0) ; Control Register
# 	        lwu r12,DR(r0) ; Data Register
# 	        daddi r10,r0,3 ; r10 = 3
# 	        s.d f0 , (r12) ; output f0
# 	        sd r10 , (r11) ;
#             daddi r1,r1,8
# 	        j printloop 
# 			nop

# exit:
#             halt
