#include <stdio.h>

#define N_SAMPLES   10
#define N_COEFFS    3

// double sample[N_SAMPLES] = {1, 2, 1, 2, 1};
double sample[N_SAMPLES] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
double coeff[N_COEFFS] = {-0.5, 1, 0.5};
double result[N_SAMPLES];

void smooth(double sample[], double coeff[], double result[], int n)
{
    int i, j;    
	double norm = 0.0;

    // Sum the absolute value of the coeff to get the norm param
    norm += (coeff[0] < 0) ? -coeff[0] : coeff[0];
    norm += (coeff[1] < 0) ? -coeff[1] : coeff[1];
    norm += (coeff[2] < 0) ? -coeff[2] : coeff[2];

    // First and last results are always the same as the samples
    result[0]   = sample[0];
    result[n-1] = sample[n-1];

    // Smooth each sample by combining it with the previos and next sample
    for (i = 1; i < n-1; i++) {
        result[i]  = sample[i-1] * coeff[0];
        result[i] += sample[i]   * coeff[1];
        result[i] += sample[i+1] * coeff[2];
        result[i] /= norm;
    }
}

int main(int argc, char *arvg[])
{
    int i;

    if (N_SAMPLES >= N_COEFFS) {
        smooth(sample, coeff, result, N_SAMPLES);
	}

	for (i = 0; i < N_SAMPLES; i++) {
        printf("%f\n", result[i]);
	}
}
